
document.getElementsByClassName("blocks-options")[0].addEventListener('click', (event) => {
    const eventTarget = event.target;
    eventTarget.classList.add('first-block');
    const itemList = document.querySelectorAll('.blocks-options li');
    for (let i = 0; i < itemList.length; i++) {
        itemList[i].classList.remove('first-block');
    }
    event.target.classList.add('first-block');

    const contentList = document.getElementsByClassName('blocks-content');
    for (let i = 0; i < contentList.length; i++) {
        contentList[i].classList.remove('first-block');
    }
    const index = eventTarget.id.substring(eventTarget.id.length - 1);
contentList[index].classList.add('first-block')
});









const buttonLoad = document.querySelector('.load-more-img');
const menuAmazingList = document.querySelectorAll('.menu-item-category');
for(let i = 0; i < menuAmazingList.length; i++) {
    menuAmazingList[i].addEventListener('click', (event) => {
        event.preventDefault();
        const eventTarget = event.target;
        const itemList = document.querySelectorAll('.menu-amazing li');
        for (let i = 0; i < itemList.length; i++) {
            itemList[i].classList.remove('selected');
        }
        eventTarget.parentNode.classList.add('selected');




        const selectedCategory = eventTarget.classList[1];
        if(selectedCategory !== 'all'){
            buttonLoad.classList.add('hidden');
        } else {
            buttonLoad.classList.remove('hidden');

        }
        const wrapperItemList = document.querySelectorAll('.wrapper-item');
        for(let i = 0; i < wrapperItemList.length; i++){
            if(wrapperItemList[i].classList.contains(selectedCategory) || selectedCategory === 'all'){
                wrapperItemList[i].classList.remove('none');
            } else {
                wrapperItemList[i].classList.add('none');
            }
        }
    })
};


const downloaderItems = document.querySelectorAll('.wrapper-item.hidden');
buttonLoad.addEventListener('click', () => {
    [...downloaderItems].forEach(element => element.classList.remove('hidden'));
    buttonLoad.remove();
} );







$('.slider-content').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    asNavFor: '.slider-nav'
});

$('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-content',
    centerMode: true,
    centerPadding: '10px',
    focusOnSelect: true,
    arrows: true,
    nextArrow: '<i class="fas fa-angle-right"></i>',
    prevArrow:  '<i class="fas fa-angle-left"></i>',

});




